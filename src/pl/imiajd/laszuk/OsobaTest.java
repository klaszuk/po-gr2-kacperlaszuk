package pl.imiajd.laszuk;

public class OsobaTest {
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Nauczyciel("Kowalski",1980, 50000);
        ludzie[1] = new Student("Nowak",1999, "informatyka");
        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+" " +p.getRokUrodzenia()+" "+p.wypisz());
        }
    }
}
