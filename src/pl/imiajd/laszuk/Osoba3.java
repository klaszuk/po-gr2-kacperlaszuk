package pl.imiajd.laszuk;

import java.time.LocalDate;
import java.util.Objects;

class Osoba3 implements Cloneable,Comparable<Osoba3> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba3(String nazwisko,LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }

    public String toString(){
        return "Osoba "+nazwisko+" "+dataUrodzenia;
    }
    public String getNazwisko3(){
        return nazwisko;
    }
    public LocalDate getDataUrodzenia3(){
        return dataUrodzenia;
    }
    public int compareTo(Osoba3 obj){
        int suma=nazwisko.compareTo(obj.nazwisko);
        suma=suma+dataUrodzenia.compareTo(obj.dataUrodzenia);
        return suma;
    }

    public boolean equals(Osoba3 obj){
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
        Osoba3 osoba=(Osoba3)obj;
        return Objects.equals(nazwisko,osoba.nazwisko) && Objects.equals(dataUrodzenia,osoba.dataUrodzenia);

    }

}
