package pl.imiajd.laszuk;

import java.time.LocalDate;

class Student2 extends Osoba2
{
    public Student2(String[] imie, String nazwisko, LocalDate dataUrodzenia, boolean plec, String kierunek, double srednia)
    {
        super(imie,nazwisko,dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.srednia=srednia;
    }

    public String getOpis()
    {
        return ("kierunek studiów: " +kierunek+" srednia: "+srednia);
    }

    public double getSrednia(){
        return srednia;
    }

    public void setSrednia(double nowa){
        this.srednia=nowa;
    }

    private String kierunek;
    private double srednia;
}
