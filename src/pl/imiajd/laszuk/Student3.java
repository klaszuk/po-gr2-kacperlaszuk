package pl.imiajd.laszuk;

import java.time.LocalDate;

class Student3 extends Osoba3 implements Cloneable,Comparable<Osoba3> {
    public Student3(String nazwisko,LocalDate dataUrodzenia,double sredniaOcen3)
    {
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen3=sredniaOcen3;
    }
    private double sredniaOcen3;

    public double getSrednia(){
        return sredniaOcen3;
    }
    public int compareTo(Student3 obj){
        if (sredniaOcen3 < obj.sredniaOcen3) {
            return -1;
        }
        if (sredniaOcen3 > obj.sredniaOcen3) {
            return 1;
        }
        return 0;
    }
}
