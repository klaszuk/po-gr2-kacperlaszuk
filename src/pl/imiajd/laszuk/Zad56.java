package pl.imiajd.laszuk;

import java.util.Stack;

public class Zad56 {
    public static void odwracaniestos(String s)
    {
        Stack<String> stc =new Stack<>();
        String temp="";
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)== ' ')
            {
                if(temp.contains("."))
                {
                    temp=temp.substring(0,temp.length()-1);
                }
                if((int)temp.charAt(0)>=65 && (int)temp.charAt(0)<=90)
                {
                    temp=temp.toLowerCase();
                    temp=temp+".";
                }
                stc.add(temp);
                temp="";
            }
            else
            {
                temp=temp+s.charAt(i);
            }

        }
        stc.add(temp);
        while(!stc.isEmpty())
        {
            temp=stc.peek();
            System.out.print(temp+" ");
            stc.pop();
        }
        System.out.println();
    }
    public static void split(int liczba)
    {
        Stack<Integer> wynik =new Stack<>();
        int temp;
        while(liczba>0)
        {
            wynik.add(liczba%10);
            liczba /=10;
        }
        while(!wynik.isEmpty())
        {
            temp=wynik.peek();
            System.out.print(temp+" ");
            wynik.pop();
        }
    }

    public static void main(String[] args)
    {
        String s = "Ala ma kota. Jej kot lubi myszy ";
        odwracaniestos(s);
        int liczba=2015;
        split(liczba);
    }
}
