package pl.imiajd.laszuk;

abstract class Osoba {
    private String nazwisko;
    private int rokUrodzenia;
    Osoba(String var1, int var2) {
        this.nazwisko = var1;
        this.rokUrodzenia = var2;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public int getRokUrodzenia(){
        return rokUrodzenia;
    }
    public abstract String wypisz();
}
class Student extends Osoba{
    private String kierunek;
    Student(String var1,int var2,String var3){
        super(var1,var2);
        this.kierunek=var3;
    }
    @Override
    public String wypisz(){
        return kierunek;
    }
}
class Nauczyciel extends Osoba{
    private int pensja;
    Nauczyciel(String var1,int var2,int var3){
        super(var1,var2);
        this.pensja=var3;
    }
    @Override
    public String wypisz(){
        return Integer.toString(pensja);
    }
}

