package pl.imiajd.laszuk;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba3 {
    public static void main(String[] args)
    {
        ArrayList<Osoba3> grupa = new ArrayList<Osoba3>();
        LocalDate datauro1=LocalDate.of(1980,12,5);
        LocalDate datauro2=LocalDate.of(2000,2,7);
        LocalDate datauro3=LocalDate.of(2000,2,7);
        LocalDate datauro4=LocalDate.of(1970,12,8);
        LocalDate datauro5=LocalDate.of(1999,5,30);
        grupa.add(new Osoba3("Nowak",datauro1));
        grupa.add(new Osoba3("Laszuk",datauro2));
        grupa.add(new Osoba3("Laszuk",datauro3));
        grupa.add(new Osoba3("Wern",datauro4));
        grupa.add(new Osoba3("Zygra",datauro5));

        System.out.println(grupa.get(1).compareTo(grupa.get(2)));

        for (Osoba3 p : grupa) {
            System.out.println(p.toString());
        }
        System.out.println("Posortowana:");
        Collections.sort(grupa);
        for (Osoba3 p : grupa) {
            System.out.println(p.toString());
        }
    }
}
