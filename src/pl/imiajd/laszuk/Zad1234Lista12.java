package pl.imiajd.laszuk;

import java.util.LinkedList;

public class Zad1234Lista12 {
    class Pracownicy
    {
        private String nazwisko;
        public Pracownicy(String nazwisko)
        {
            this.nazwisko=nazwisko;
        }
    }
    public static <T extends Comparable> void redukuj(LinkedList<T> pracownicy,int n)
    {
        for(int i=n-1;i<pracownicy.size();)
        {
            pracownicy.remove(i);
            i=i+n-1;
        }
    }
    public static <T extends Comparable> void odwroc(LinkedList<T> pracownicy)
    {
        LinkedList<T>wynik=new LinkedList<T>();
        for(int i=pracownicy.size()-1;i>=0;i--)
        {
            wynik.add(pracownicy.get(i));
        }
        pracownicy.removeAll(pracownicy);
        for(int i=0;i<wynik.size();i++)
        {
            pracownicy.add(wynik.get(i));
        }
    }

    public static void main(String[] args)
    {
        LinkedList<String> pracownicy=new LinkedList<>();
        pracownicy.add("Marek");
        pracownicy.add("Tomek");
        pracownicy.add("Kacper");
        pracownicy.add("Szymek");
        pracownicy.add("Marta");
        pracownicy.add("Ola");
        //redukuj(pracownicy,3);
        odwroc(pracownicy);
        System.out.println(pracownicy);

    }
}
