package pl.imiajd.laszuk;

import java.time.LocalDate;

public class ArrayUtil
{
    public static void main(String args[])
    {
        Integer[] a = {10, 9, 7, 5, 10, 15};
        System.out.println(ArraySort.<Integer>isSorted(a));
        LocalDate datauro1=LocalDate.of(1980,12,5);
        LocalDate datauro2=LocalDate.of(2000,2,7);
        LocalDate datauro3=LocalDate.of(2001,2,7);
        LocalDate datauro4=LocalDate.of(2200,12,8);
        LocalDate datauro5=LocalDate.of(2210,5,30);
        LocalDate[] b = {datauro1, datauro2, datauro3, datauro4, datauro5};
        System.out.println(ArraySort.<LocalDate>isSorted(b));
        ArraySort.<Integer>selectionSort(a);
        System.out.println(ArraySort.<Integer>binSearch(a,7));

    }

}

class ArraySort
{
    public static <T extends Comparable> String isSorted(T[] a)
    {
        if (a == null || a.length == 0) {
            return "Taka tablica nie istnieje";
        }


        for (int j = 0; j < a.length-1; ++j) {
            if (a[j].compareTo(a[j+1]) > 0) {
                return "Nie jest niemalejąca";
            }
        }
        return "Jest niemalejąca";
    }

    public static <T extends Comparable> int binSearch(T[]a ,int szukana)
    {
        int lewo=0;
        int prawo=a.length-1;
        int przeszukiwany;
        while (lewo<=prawo)
        {
            przeszukiwany=(int)Math.floor((lewo+prawo)/2);
            if(a[przeszukiwany].compareTo(szukana)<0)
            {
                lewo=przeszukiwany+1;
            }
            else if(a[przeszukiwany].compareTo(szukana)>0)
            {
                prawo=przeszukiwany-1;
            }
            else
            {
                return przeszukiwany;
            }
        }
        return -1;
    }
    public static <T extends Comparable> void selectionSort(T[]a)
    {
        for(int i=0;i<a.length-1;i++)
        {
            T min=a[i];
            int indeks=i;
            for(int j=i+1;j<a.length-2;j++)
            {
                if(min.compareTo(a[j])>0)
                {
                    min=a[j];
                    indeks=j;
                }
            }
            T pomoc=a[i];
            a[i]=min;
            a[indeks]=pomoc;
        }
    }
}

