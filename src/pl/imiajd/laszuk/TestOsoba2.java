package pl.imiajd.laszuk;

import java.util.*;
import java.time.LocalDate;

public class TestOsoba2
{
    public static void main(String[] args)
    {
        Osoba2[] ludzie = new Osoba2[2];
        String[] imiona1={"Karol","Jacek"};
        String[] imiona2={"Marta"};
        LocalDate datauro1=LocalDate.of(1980,12,5);
        LocalDate datauro2=LocalDate.of(2000,2,7);
        LocalDate datazatru=LocalDate.of(2010,8,30);
        ludzie[0] = new Pracownik2(imiona1,"Kowalski",datauro1, true,5000,datazatru);
        ludzie[1] = new Student2(imiona2, "Nowak",datauro2,false,"informatyka",4.53);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba2 p : ludzie) {
            System.out.println(p.getImie() +" "+ p.getNazwisko()+" "+p.getDateUrodzenia() +" "+p.getPlec()+ ": " + p.getOpis());
        }
    }
}

