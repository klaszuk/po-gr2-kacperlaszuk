package pl.imiajd.laszuk;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Czytaj10 {

    public static void main(String[] args) {
        ArrayList<String> zpliku = new ArrayList<String>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "C:\\Users\\irisk\\IdeaProjects\\po-gr2-kacperlaszuk\\src\\pl\\imiajd\\laszuk\\lista10dane.txt"));
            String line = reader.readLine();
            while (line != null) {
                zpliku.add(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(zpliku);
        for(String x : zpliku) {
            System.out.println(x);
        }
    }
}
