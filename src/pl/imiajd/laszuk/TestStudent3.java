package pl.imiajd.laszuk;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent3 {
    public static void main(String[] args)
    {
        ArrayList<Student3> grupa2 = new ArrayList<Student3>();
        LocalDate datauro1=LocalDate.of(1998,12,5);
        LocalDate datauro2=LocalDate.of(2000,2,7);
        LocalDate datauro3=LocalDate.of(2000,2,7);
        LocalDate datauro4=LocalDate.of(1999,12,8);
        LocalDate datauro5=LocalDate.of(2001,5,30);
        grupa2.add(new Student3("Nowak",datauro1,4.5));
        grupa2.add(new Student3("Laszuk",datauro2,3));
        grupa2.add(new Student3("Wern",datauro3,3));
        grupa2.add(new Student3("Wern",datauro4,3.5));
        grupa2.add(new Student3("Zygra",datauro5,4.5));

        for (Student3 p : grupa2) {
            System.out.println(p.toString()+"Srednia "+p.getSrednia());
        }
        System.out.println("Posortowana:");
        Collections.sort(grupa2);
        for (Student3 p : grupa2) {
            System.out.println(p.toString()+"Srednia "+p.getSrednia());
        }
    }
}
