package pl.imiajd.laszuk;

import java.time.LocalDate;

abstract class Osoba2
{
    public Osoba2(String[] imie, String nazwisko, LocalDate dataUrodzenia, boolean plec)
    {
        this.imie = imie;
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String getImie()
    {
        StringBuilder pomoc = new StringBuilder();
        for(String s : imie) {
            pomoc.append(s);
            pomoc.append(" ");
        }
        String str = pomoc.toString();
        return str;
    }

    public LocalDate getDateUrodzenia(){
        return dataUrodzenia;
    }

    public String getPlec(){
        if(plec==true){
            return "Mezczyzna";
        }
        else {
            return "Kobieta";
        }
    }

    private String nazwisko;
    private String[] imie;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
