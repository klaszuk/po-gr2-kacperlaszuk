package pl.imiajd.laszuk;

import java.time.LocalDate;

class Pracownik2 extends Osoba2
{
    public Pracownik2(String[] imie, String nazwisko, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(imie,nazwisko,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public LocalDate getDataZatrudnienia()
    {
        return dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł ", pobory)+"data zatrudnienia: " +dataZatrudnienia;
    }


    private double pobory;
    private LocalDate dataZatrudnienia;
}


