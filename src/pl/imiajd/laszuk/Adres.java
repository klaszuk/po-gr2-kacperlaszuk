package pl.imiajd.laszuk;

 class Adres {
     private String ulica;
     private int numer_domu;
     private int numer_mieszkania;
     private String miasto;
     private String kod_pocztowy;

      Adres(String var1,int var2,int var3,String var4,String var5) {
         this.ulica = var1;
         this.numer_domu=var2;
         this.numer_mieszkania=var3;
         this.miasto=var4;
         this.kod_pocztowy=var5;
     }
     Adres(String var1,int var2,String var4,String var5) {
         this.ulica = var1;
         this.numer_domu=var2;
         this.miasto=var4;
         this.kod_pocztowy=var5;
     }
     public void wypiszInfo(){
          System.out.println(this.kod_pocztowy);
          System.out.println(this.miasto);
          System.out.print(this.ulica+" ");
          System.out.print(this.numer_domu+" ");
          System.out.print(this.numer_mieszkania);
     }

}
