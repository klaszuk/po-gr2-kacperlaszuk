package pl.edu.uwm.wmii.kotewa.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie2 {

    public static void main(String[] args) {
        ArrayList<Integer> tab1=new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        ArrayList<Integer> tab2=new ArrayList<>(Arrays.asList(6, 6, 6));
        ArrayList<Integer> tab3=merge(tab1,tab2);
        for(int i=0;i<tab3.size();i++)
        {
            System.out.print(tab3.get(i));
        }
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> tab1,ArrayList<Integer> tab2){
        ArrayList<Integer> wynik=new ArrayList<>();
        int po1=0;
        int po2=0;
        int i=0;
        for(;i<tab1.size()+tab2.size();){
            if(po1<tab1.size()){
                wynik.add(i,tab1.get(po1));
                po1=po1+1;
                i=i+1;
            }
            if(po2<tab2.size()){
                wynik.add(i,tab2.get(po2));
                po2=po2+1;
                i=i+1;
            }
        }
        return wynik;
    }
}

