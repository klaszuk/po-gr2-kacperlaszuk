package pl.edu.uwm.wmii.kotewa.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie3 {

    public static void main(String[] args) {
        ArrayList<Integer> tab1=new ArrayList<>(Arrays.asList(2, 3, 9, 4, 13));
        ArrayList<Integer> tab2=new ArrayList<>(Arrays.asList(1, 2, 9, 5));
        ArrayList<Integer> tab3=mergeSorted(tab1,tab2);
        for(int i=0;i<tab3.size();i++)
        {
            System.out.print(tab3.get(i));
        }
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> tab1,ArrayList<Integer> tab2){
        ArrayList<Integer> wynik=new ArrayList<>();
        int i=0;
        for(;i<tab1.size();i++){
            wynik.add(i,tab1.get(i));
        }
        for(int k=0;k<tab2.size();k++,i++){
            wynik.add(i,tab2.get(k));
        }
        for(int p=0;p<wynik.size();p++){
            for(int j=0;j<=wynik.size()-2;j++){
                if(wynik.get(j)>wynik.get(j+1)){
                    int pomoc1=wynik.get(j);
                    wynik.set(j,wynik.get(j+1));
                    wynik.set(j+1,pomoc1);
                }
            }
        }
        return wynik;
    }
}
