package pl.edu.uwm.wmii.kotewa.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie1 {

    public static void main(String[] args) {
        ArrayList<Integer> tab1=new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> tab2=new ArrayList<>(Arrays.asList(6, 6, 6));
        ArrayList<Integer> tab3=append(tab1,tab2);
        for(int i=0;i<tab3.size();i++)
        {
            System.out.print(tab3.get(i));
        }
    }
    public static ArrayList<Integer> append(ArrayList<Integer> tab1,ArrayList<Integer> tab2){
        ArrayList<Integer> wynik=new ArrayList<>();
        int i=0;
        for(;i<tab1.size();i++){
            wynik.add(i,tab1.get(i));
        }
        for(int k=0;k<tab2.size();k++,i++){
            wynik.add(i,tab2.get(k));
        }
        return wynik;
    }
}
