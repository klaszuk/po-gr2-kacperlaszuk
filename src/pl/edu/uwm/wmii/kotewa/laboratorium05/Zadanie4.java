package pl.edu.uwm.wmii.kotewa.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie4 {

    public static void main(String[] args) {
        ArrayList<Integer> tab1=new ArrayList<>(Arrays.asList(2, 3, 9, 4, 13));
        ArrayList<Integer> tab3=reversed(tab1);
        for(int i=0;i<tab3.size();i++)
        {
            System.out.print(tab3.get(i));
        }
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> tab1){
        ArrayList<Integer> wynik=new ArrayList<>();
        for(int i=tab1.size()-1;i>=0;i--){
            wynik.add(tab1.size()-1-i,tab1.get(i));
        }
        return wynik;
    }
}

