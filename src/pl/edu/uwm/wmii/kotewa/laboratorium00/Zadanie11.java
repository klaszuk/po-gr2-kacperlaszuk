package pl.edu.uwm.wmii.kotewa.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
        System.out.println("Wisława Szymborska PRÓBA");
        System.out.println(" ");
        System.out.println("Oj tak, piosenko, szydzisz ze mnie,");
        System.out.println("bo choćbym poszła górą, nie zakwitną różą.");
        System.out.println("Różą zakwita róża i nikt inny. Wiesz.");
        System.out.println("Próbowałam mieć liście. Chciałam się zakrzewić.");
        System.out.println("Z oddechem powstrzymanym - żeby było prędzej -");
        System.out.println("oczekiwałam chwili zamknięcia się w róży.");
        System.out.println("Piosenko, która nie znasz nade mną litości:");
        System.out.println("mam ciało pojedyncze, nieprzemienne w nic,");
        System.out.println("jestem jednorazowa aż do szpiku kości.");
    }
}
