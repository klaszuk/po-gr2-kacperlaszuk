package pl.edu.uwm.wmii.kotewa.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;

public class Cw2Zadanie3 {
    public static void main(String[] args) {
        int suma=0;
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        for(int i=0;i<n;i++) {
            Scanner scan2=new Scanner(System.in);
            int wp=scan2.nextInt();
            if((sqrt(wp))%2==0) {
                suma=suma+1;
            }
        }
        System.out.println(suma);
    }
}