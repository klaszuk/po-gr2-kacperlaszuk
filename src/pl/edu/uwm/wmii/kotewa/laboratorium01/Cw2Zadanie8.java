package pl.edu.uwm.wmii.kotewa.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;

public class Cw2Zadanie8 {
    public static void main(String[] args) {
        int suma=0;
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        ArrayList<Integer> lista = new ArrayList<Integer>();
        for(int k=1;k<=n;k++) {
            Scanner scan2=new Scanner(System.in);
            int wp=scan2.nextInt();
            lista.add(wp);
            if(abs(wp)<pow(k,2)) {
                suma=suma+1;
            }
        }
        System.out.println(suma);
    }
}
