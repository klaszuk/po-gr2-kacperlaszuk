package pl.edu.uwm.wmii.kotewa.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;

public class ZadanieNr2 {
    public static void main(String[] args) {
        float suma=0;
        Scanner scan=new Scanner(System.in);
        float n=scan.nextFloat();
        ArrayList<Float> lista = new ArrayList<Float>();
        for(int k=1;k<=n;k++) {
            Scanner scan2=new Scanner(System.in);
            float wp=scan2.nextFloat();
            lista.add(wp);
            if(wp*(-1)<0) {
                suma=suma+(wp*2);
            }
            else {
                suma=suma+wp;
            }
        }
        System.out.println(suma);
    }
}
