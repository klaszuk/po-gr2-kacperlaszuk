package pl.edu.uwm.wmii.kotewa.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;

public class Cw2Zadanie6 {
    public static void main(String[] args) {
        int suma=0;
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        ArrayList<Integer> lista = new ArrayList<Integer>();
        for(int i=1;i<=n;i++) {
            Scanner scan2=new Scanner(System.in);
            int wp=scan2.nextInt();
            lista.add(wp);
            if(i%2==1 && wp%2==0) {
                suma=suma+1;
            }
        }
        System.out.println(suma);
    }
}

