package pl.edu.uwm.wmii.kotewa.laboratorium01;

import static java.lang.Math.*;
import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        double suma=0;
        double silnia=1;
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();

        for(int i=1;i<=n;i++) {
            Scanner scan2=new Scanner(System.in);
            double wp=scan2.nextDouble();
            silnia=silnia*i;
            suma=suma+((pow(-1,i)*wp))/silnia;
        }
        System.out.println(suma);
    }
}