package pl.edu.uwm.wmii.kotewa.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;

public class ZadanieNr3 {
    public static void main(String[] args) {
        int dod=0;
        int uje=0;
        int zer=0;
        Scanner scan=new Scanner(System.in);
        float n=scan.nextFloat();
        ArrayList<Float> lista = new ArrayList<Float>();
        for(int k=1;k<=n;k++) {
            Scanner scan2=new Scanner(System.in);
            float wp=scan2.nextFloat();
            lista.add(wp);
            if(wp<0) {
                uje=uje+1;
            }
            else if(wp>0) {
                dod=dod+1;
            }
            else {
                zer=zer+1;
            }
        }
        System.out.println("Ujemne:" +uje);
        System.out.println("Dodatnie:" +dod);
        System.out.println("Zera:" +zer);
    }
}
