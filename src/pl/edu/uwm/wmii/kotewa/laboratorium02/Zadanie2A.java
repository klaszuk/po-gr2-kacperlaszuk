package pl.edu.uwm.wmii.kotewa.laboratorium02;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;
import java.util.Random;
import java.lang.Math;

public class Zadanie2A {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int n=scan1.nextInt();
        Scanner scan2=new Scanner(System.in);
        int min=scan2.nextInt();
        Scanner scan3=new Scanner(System.in);
        int max=scan3.nextInt();
        int[] tab=new int[n];
        generuj(tab,n,min,max);
        int nie=ileNieparzystych(tab);
        int parz=ileParzystych(tab);
        System.out.println(nie);
        System.out.println(parz);

    }
    public static void generuj(int tab[],int n,int min,int max) {
        int range=max-(min)+1;
        for(int k=0;k<n;k++) {
            int los=(int)(Math.random()*range)+(min);
            tab[k]=los;
            System.out.println(tab[k]);
        }
    }
    public static int ileNieparzystych(int tab[]) {
        int ilen=0;
        for(int k=0;k<tab.length;k++) {
            if(abs(tab[k])%2==1) {
                ilen=ilen+1;
            }
        }
        return ilen;
    }
    public static int ileParzystych(int tab[]) {
        int ilep=0;
        for(int k=0;k<tab.length;k++) {
            if(abs(tab[k])%2==0) {
                ilep=ilep+1;
            }
        }
        return ilep;
    }
}




