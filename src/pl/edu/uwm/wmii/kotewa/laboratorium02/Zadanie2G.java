package pl.edu.uwm.wmii.kotewa.laboratorium02;

import java.util.Arrays;
import java.util.Scanner;
import java.lang.Math;

public class Zadanie2G {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int n=scan1.nextInt();
        Scanner scan2=new Scanner(System.in);
        int min=scan2.nextInt();
        Scanner scan3=new Scanner(System.in);
        int max=scan3.nextInt();
        int[] tab=new int[n];
        Scanner scan4=new Scanner(System.in);
        int lewa=scan4.nextInt();
        Scanner scan5=new Scanner(System.in);
        int prawa=scan5.nextInt();
        generuj(tab,n,min,max);
        odwrocFragment(tab,lewa,prawa);

    }
    public static void generuj(int tab[],int n,int min,int max) {
        int range=max-(min)+1;
        for(int k=0;k<n;k++) {
            int los=(int)(Math.random()*range)+(min);
            tab[k]=los;
            System.out.println(tab[k]);
        }
    }
    public static void odwrocFragment(int tab[],int lewa,int prawa) {
        int[] tab2 = Arrays.copyOf(tab, tab.length);
        int licznik=0;
        for(int k=lewa;k<=prawa;k++) {
            tab[k]=tab2[prawa-licznik];
            licznik=licznik+1;
        }
        for(int k=0;k<tab.length;k++) {
            System.out.println(tab[k]);
        }
    }
}


















