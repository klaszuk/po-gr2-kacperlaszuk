package pl.edu.uwm.wmii.kotewa.laboratorium02;

import java.util.Scanner;
import static java.lang.Math.*;
import java.util.*;
import java.util.Random;
import java.lang.Math;

public class Zadanie2B {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int n=scan1.nextInt();
        Scanner scan2=new Scanner(System.in);
        int min=scan2.nextInt();
        Scanner scan3=new Scanner(System.in);
        int max=scan3.nextInt();
        int[] tab=new int[n];
        generuj(tab,n,min,max);
        int dod=ileDodatnich(tab);
        int uje=ileUjemnych(tab);
        int zer=ileZerowych(tab);
        System.out.println(dod);
        System.out.println(uje);
        System.out.println(zer);

    }
    public static void generuj(int tab[],int n,int min,int max) {
        int range=max-(min)+1;
        for(int k=0;k<n;k++) {
            int los=(int)(Math.random()*range)+(min);
            tab[k]=los;
            System.out.println(tab[k]);
        }
    }
    public static int ileDodatnich(int tab[]) {
        int ile=0;
        for(int k=0;k<tab.length;k++) {
            if(tab[k]>0) {
                ile=ile+1;
            }
        }
        return ile;
    }
    public static int ileUjemnych(int tab[]) {
        int ile=0;
        for(int k=0;k<tab.length;k++) {
            if(tab[k]<0) {
                ile=ile+1;
            }
        }
        return ile;
    }
    public static int ileZerowych(int tab[]) {
        int ile=0;
        for(int k=0;k<tab.length;k++) {
            if(tab[k]==0) {
                ile=ile+1;
            }
        }
        return ile;
    }
}





