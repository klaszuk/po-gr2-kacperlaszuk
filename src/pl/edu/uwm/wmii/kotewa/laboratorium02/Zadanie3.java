package pl.edu.uwm.wmii.kotewa.laboratorium02;

import java.util.Scanner;
import java.lang.Math;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int m=scan1.nextInt();
        Scanner scan2=new Scanner(System.in);
        int n=scan2.nextInt();
        Scanner scan3=new Scanner(System.in);
        int k=scan3.nextInt();
        if(m>10 || m<0 || n>10 || n<0 || k>10 || k<0) {
            return;
        }
        int[][] a=new int[m][n];
        int[][] b=new int[n][k];
        generuj(a,b,m,n,k);
        wypisz(a);
        wypisz(b);
        int c[][]=iloczyn(a,b,m,n,k);
        wypisz(c);

    }
    public static void generuj(int a[][],int b[][],int m,int n,int k) {
        int range=10-(-10)+1;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                int los=(int)(Math.random()*range)+(-10);
                a[i][j]=los;
            }
        }
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                int los=(int)(Math.random()*range)+(-10);
                b[i][j]=los;
            }
        }
    }
    public static void wypisz(int a[][]) {
        for(int[] x:a){
            System.out.print("[");
            for(int y:x){
                System.out.print(y+" ");
            }
            System.out.print("]");
            System.out.println();
        }
        System.out.println();
    }
    public static int[][] iloczyn(int a[][],int b[][],int m,int n,int k) {
        int c[][]= new int[n][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                c[i][j] = 0;
            }
        }
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                for(int p=0;k<n;p++) {
                    c[i][j]+=a[i][p]*b[p][j];
                }
            }
        }
        return c;
    }
}



















