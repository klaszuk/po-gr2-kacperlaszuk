package pl.edu.uwm.wmii.kotewa.laboratorium06;

class RachunekBankowy {
    public static double rocznaStopaProcentowa;
    private double saldo;
    public RachunekBankowy(double sp,int s)
    {
        rocznaStopaProcentowa=sp;
        saldo=s;
    }
    public double ObliczMiesieczneOdsetki(){
        return (saldo*rocznaStopaProcentowa/12);
    }
    public void setRocznaStopaProcentowa(){
        saldo=saldo+ObliczMiesieczneOdsetki();
    }

    public static void main(String[] args) {
        double r=0.04;
        RachunekBankowy saver1=new RachunekBankowy(r,2000);
        RachunekBankowy saver2=new RachunekBankowy(r,3000);
        double wynik1= saver1.ObliczMiesieczneOdsetki();
        double wynik2= saver2.ObliczMiesieczneOdsetki();
        System.out.println(wynik1);
        System.out.println(wynik2);
        saver1.setRocznaStopaProcentowa();
        saver2.setRocznaStopaProcentowa();
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        RachunekBankowy.rocznaStopaProcentowa=0.05;
        System.out.println(saver1.rocznaStopaProcentowa);
        double wynik3= saver1.ObliczMiesieczneOdsetki();
        double wynik4= saver2.ObliczMiesieczneOdsetki();
        System.out.println(wynik3);
        System.out.println(wynik4);
        saver1.setRocznaStopaProcentowa();
        saver2.setRocznaStopaProcentowa();
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
    }

}
