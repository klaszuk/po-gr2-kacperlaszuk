package pl.edu.uwm.wmii.kotewa.laboratorium06;

import java.util.ArrayList;
import java.util.Arrays;
import java.lang.*;

public class IntegerSet {
    public static boolean[] zbior ={false,false,false,false};
    private int[] tab1;
    public IntegerSet(boolean[]z,int[] a,int [] b)
    {
        zbior=z;
        tab1=a;
    }
    public static int[]union(int[]a ,int[]b){
        ArrayList<Integer> wynik=new ArrayList<>();
        int licznik=0;
        for(int i=0;i<a.length;i++){
            wynik.add(licznik,a[i]);
            licznik=licznik+1;
        }
        for(int j=0;j<b.length;j++) {
            int pomoc=0;
            for(int k=0;k<a.length;k++){
                if(b[j]!= wynik.get(k)){
                    pomoc=pomoc+1;
                }
            }
            if(pomoc==a.length)
            {
                wynik.add(licznik,b[j]);
            }
            pomoc=0;
        }
        int[] wynik1 =new int[wynik.size()];
        for (int i=0; i < wynik1.length; i++)
        {
            wynik1[i] = wynik.get(i).intValue();
        }
        return wynik1;
    }

    public static int[] intersection(int[]a ,int[]b){
        ArrayList<Integer> wynik=new ArrayList<>();
        int licznik=0;
        for(int i=0;i<a.length;i++){
            for(int j=0;j<b.length;j++){
                if(a[i]==b[j]){
                    wynik.add(licznik,a[i]);
                    licznik=licznik+1;
                }
            }
        }
        int[] wynik1 =new int[wynik.size()];
        for (int i=0; i < wynik1.length; i++)
        {
            wynik1[i] = wynik.get(i).intValue();
        }
        return wynik1;
    }
    public void insertElement(int c){
        ArrayList<Integer> wynik=new ArrayList<>();
        int licznik=0;
        for(int i=0;i< tab1.length;i++){
            wynik.add(licznik,tab1[i]);
            licznik=licznik+1;
        }
        wynik.add(licznik,c);
        int[] wynik1 =new int[wynik.size()];
        for (int i=0; i < wynik1.length; i++)
        {
            wynik1[i] = wynik.get(i).intValue();
        }
        tab1=wynik1;
        for(int i:tab1){
            System.out.print(i+" ");
        }
    }
    public void deleteElement(int c){
        ArrayList<Integer> wynik=new ArrayList<>();
        int licznik=0;
        for(int i=0;i< tab1.length;i++){
            if(tab1[i]!=c){
                wynik.add(licznik,tab1[i]);
                licznik=licznik+1;
            }
        }
        int[] wynik1 =new int[wynik.size()];
        for (int i=0; i < wynik1.length; i++)
        {
            wynik1[i] = wynik.get(i).intValue();
        }
        tab1=wynik1;
        for(int i:tab1){
            System.out.print(i+" ");
        }
    }
    public String toString(){
        String wynik1= Arrays.toString(tab1).replaceAll("\\[|\\]", "");
        String wynik2=wynik1.replace(",","");
        return wynik2;
    }
    public int equals(int[] b){
        if(tab1.length!=b.length){
            return 0;
        }
        for(int i=0;i<tab1.length;i++)
        {
            if(tab1[i]!=b[i]){
                return 0;
            }
        }
        return 1;
    }
    public static void main(String[] args) {
        int[] tab1={1,2,3,4,5};
        int[] tab2={9,3,8,4,5,5,5};
        boolean[] tab3={false,false,false,false};
        IntegerSet proba=new IntegerSet(tab3,tab1,tab2);
        int[] wynik=intersection(tab1,tab2);
        for(int i:wynik){
            System.out.print(i+" ");
        }
        System.out.println();
        int[] wynik2=union(tab1,tab2);
        for(int i:wynik2){
            System.out.print(i+" ");
        }
        System.out.println();
        proba.insertElement(8);
        System.out.println();
        proba.deleteElement(2);
        System.out.println();
        String wynik4=proba.toString();
        System.out.println(wynik4);
        int wynik5= proba.equals(tab2);
        System.out.println(wynik5);
    }
}
