package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.util.Scanner;
import java.lang.*;

public class Zadanie1f {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        String str=scan1.nextLine();
        System.out.println(str);
        String wynik=change(str);
        System.out.println(wynik);
    }
    public static String change(String str) {
        StringBuffer wynik=new StringBuffer(str.length());
        for(int i=0;i<str.length();i++){
            if((int)str.charAt(i)>=65 && (int)str.charAt(i)<=95){
                int pomoc=(int)str.charAt(i)+32;
                wynik.append((char)pomoc);
            }
            if((int)str.charAt(i)>=97 && (int)str.charAt(i)<=122){
                int pomoc=(int)str.charAt(i)-32;
                wynik.append((char)pomoc);
            }
        }
        return wynik.toString();
    }
}


