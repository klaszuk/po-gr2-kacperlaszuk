package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.util.Scanner;
import java.lang.*;

public class Zadanie1h {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        String str=scan1.nextLine();
        System.out.println(str);
        Scanner scan2=new Scanner(System.in);
        char separator=scan2.next().charAt(0);
        System.out.println(separator);
        Scanner scan3=new Scanner(System.in);
        int miejsca=scan3.nextInt();
        System.out.println(miejsca);
        String wynik=nice(str,separator,miejsca);
        System.out.println(wynik);
    }
    public static String nice(String str,char separator,int miejsca) {
        StringBuffer wynik=new StringBuffer();
        int licznik=0;
        for(int i=str.length()-1;i>=0;i--){
            licznik=licznik+1;
            wynik.append(str.charAt(i));
            if(licznik%miejsca==0 && i!=0){
                wynik.append(separator);
            }
        }
        wynik.reverse();
        return wynik.toString();
    }
}




