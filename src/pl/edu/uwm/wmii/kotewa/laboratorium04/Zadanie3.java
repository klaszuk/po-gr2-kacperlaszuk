package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Zadanie3
{
    public static void main(String[] args) throws IOException
    {
        File f1=new File("C:\\Users\\irisk\\IdeaProjects\\po-gr2-kacperlaszuk\\src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium04\\Zadanie2.txt");
        Scanner scan1=new Scanner(System.in);
        String str=scan1.nextLine();
        System.out.println(str);
        int wynik=liczWyrazy(str,f1);
        System.out.println(wynik);

    }
    public static int liczWyrazy(String slowo,File path) throws IOException {
        String[] words=null;
        int wc=0;
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        String s;
        while((s=br.readLine())!=null)
        {
            words=s.split(" ");
            wc=wc+words.length;
        }
        int suma=0;
        for(int i=0;i< words.length;i++){
            if(words[i]==slowo){
                suma=suma+1;
            }
        }
        fr.close();
        return suma;
    }
}