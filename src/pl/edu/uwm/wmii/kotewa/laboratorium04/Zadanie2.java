package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Zadanie2 {
    private static final String FILE_PATH = "C:\\Users\\irisk\\IdeaProjects\\po-gr2-kacperlaszuk\\src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium04\\Zadanie2.txt";
    public static void main(String args[]) throws IOException {
        FileUtil fileUtil = new FileUtil(FILE_PATH);
        Scanner scan1=new Scanner(System.in);
        char litera=scan1.next().charAt(0);
        System.out.println(litera);
        System.out.println(fileUtil.getCharCount(litera));
    }
}

class FileUtil {
    static BufferedReader reader = null;
    public FileUtil(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        reader = new BufferedReader(input);
    }

    public static int getCharCount(char litera) throws IOException {
        int licznik = 0;
        String data;
        int suma=0;
        while((data = reader.readLine()) != null) {
            licznik += data.length();
            for(int i=0;i<data.length();i++){
                if(data.charAt(i)==litera){
                    suma=suma+1;
                }
            }
        }
        return suma;
    }
}
