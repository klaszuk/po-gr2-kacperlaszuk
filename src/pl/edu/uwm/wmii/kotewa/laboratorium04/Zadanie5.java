package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.math.RoundingMode;
import java.util.Scanner;
import java.math.BigDecimal;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int k=scan1.nextInt();
        System.out.println(k);
        Scanner scan2=new Scanner(System.in);
        float p=scan2.nextFloat();
        System.out.println(p);
        Scanner scan3=new Scanner(System.in);
        int n=scan3.nextInt();
        System.out.println(n);
        BigDecimal wynik=Kapital(k,p,n);
        System.out.println(wynik);

    }
    public static BigDecimal Kapital(int k,float p,int n) {
        BigDecimal wynik=new BigDecimal(k);
        for(int i=0;i<n;i++){
            wynik=wynik.add(new BigDecimal(k*p));
        }
        wynik = wynik.setScale(2, RoundingMode.CEILING);
        return wynik;
    }
}
