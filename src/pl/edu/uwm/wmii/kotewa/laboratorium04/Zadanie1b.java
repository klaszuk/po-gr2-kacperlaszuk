package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        String str=scan1.nextLine();
        System.out.println(str);
        Scanner scan2=new Scanner(System.in);
        String subStr=scan2.nextLine();
        System.out.println(subStr);
        int suma=countSubStr(str,subStr);
        System.out.println(suma);

    }
    public static int countSubStr(String str,String subStr) {
        int position = 0;
        int suma = 0;
        int n = subStr.length();
        while ((position = str.indexOf(subStr, position)) != -1) {
            position = position + n;
            suma++;
        }
        return suma;
    }
}

