package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.util.Scanner;
import java.lang.*;

public class Zadanie1g {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        String str=scan1.nextLine();
        System.out.println(str);
        String wynik=nice(str);
        System.out.println(wynik);
    }
    public static String nice(String str) {
        StringBuffer wynik=new StringBuffer();
        int licznik=0;
        for(int i=str.length()-1;i>=0;i--){
            licznik=licznik+1;
            wynik.append(str.charAt(i));
            if(licznik%3==0 && i!=0){
                wynik.append(",");
            }
        }
        wynik.reverse();
        return wynik.toString();
    }
}



