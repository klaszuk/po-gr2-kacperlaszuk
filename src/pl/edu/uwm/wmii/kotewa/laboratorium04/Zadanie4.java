package pl.edu.uwm.wmii.kotewa.laboratorium04;

import java.util.Scanner;
import java.math.BigInteger;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scan1=new Scanner(System.in);
        int n=scan1.nextInt();
        System.out.println(n);
        BigInteger wynik=Szachownica(n);
        System.out.println(wynik);
    }
    public static BigInteger Szachownica(int n) {
        BigInteger wynik=new BigInteger("0");
        BigInteger ziarko=new BigInteger("1");
        for(int i=1;i<=n*n;i++){
            wynik=wynik.add(ziarko);
            ziarko=ziarko.multiply(new BigInteger("2"));
        }
        return wynik;
    }
}