package kolokwium02;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

abstract class Serwis {
    LocalDateTime czas;
    protected double cena;

    Serwis(LocalDateTime czas)
    {
        this.czas=czas;
    }

    public Serwis() {
    }

    public void obliczCene1()
    {
    }

    public String toString2()
    {
        return "Koszt polaczenia:"+cena+" Data i czas"+czas;
    }

}

class Polaczenie extends Serwis{
    private String numer;
    private int czasP;

    @Override
    public void obliczCene1()
    {
        this.cena=this.czasP*0.18;
    }

    Polaczenie(LocalDateTime var1,String var2,int var3)
    {
        super(var1);
        this.numer=var2;
        this.czasP=var3;
        obliczCene1();
    }

    @Override
    public String toString2()
    {
        return "Połączenie: numer "+numer+", data i godzina rozmowy: "+czas+", długość trwania: "+czasP+",łączny koszt:"+cena;
    }
}

class Sms extends Serwis {
    private String numer;

    @Override
    public void obliczCene1() {
        this.cena = 0.08;
    }

    Sms(LocalDateTime var1, String var2) {
        super(var1);
        this.numer = var2;
        obliczCene1();
    }

    @Override
    public String toString2() {
        return "Sms: numer " + numer + ", data i godzina smsa: " + czas + ",łączny koszt:" + cena;
    }
}

class Internet extends Serwis{
    private int iloscMB;

    @Override
    public void obliczCene1() {
        this.cena=Math.round((this.iloscMB/775)*100)/100;
    }

    Internet(LocalDateTime var1,int var2)
    {
        super(var1);
        this.iloscMB=var2;
        obliczCene1();
    }

    @Override
    public String toString2() {
        return "Internet, data i godzina internetu: " + czas +", iloscMB" +iloscMB+ ", łączny koszt:" + cena;
    }

}

interface IBiling{
    public void ZapiszBiling() throws IOException;
}
interface IDodaj{
    public void DodajPolaczenie(String numer,int czasP);
    public void DodajSms(String numer);
    public void DodajInternet(int iloscMB);
}

class Telefon implements IBiling,IDodaj{
    private List<Serwis> biling=new LinkedList<Serwis>();
    public void DodajPolaczenie(String numer,int czasP)
    {
        biling.add(new Polaczenie(LocalDateTime.now(),numer,czasP));
    }
    public void DodajSms(String numer)
    {
        biling.add(new Sms(LocalDateTime.now(),numer));
    }
    public void DodajInternet(int iloscMB)
    {
        biling.add(new Internet(LocalDateTime.now(),iloscMB));
    }

    public String toString2()
    {
        String pusty="";
        for(int i=0;i<biling.size();i++)
        {
            pusty=pusty+biling+"\n";
        }
        return pusty;
    }

    public void ZapiszBiling ()throws IOException
    {
        PrintWriter out = new PrintWriter("biling.txt");
        out.println(toString2());
    }



}

